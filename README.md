iotuzriffic
===========

Teaching myself the ESP IDF with a from-scratch
[IoTuz firmware](https://github.com/CCHS-Melbourne/iotuz-esp32-firmware).
